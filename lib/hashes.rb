# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = str.split(' ')
  words_length = Hash.new
  words.each { |ele| words_length[ele] = ele.length }
  words_length
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  sorted = hash.sort_by {|k,v| v}
  sorted[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k,v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = {}
  word.chars do |char|
    if hash[char] == nil
      hash[char] = 1
    else
      hash[char] += 1
    end
  end
  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  digits = "1234567890"
  hash = letter_counts(arr.to_s)
  result = []
  hash.each { |k,v| result << k.to_i if digits.include?(k)}
  result
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  result = {:even => 0, :odd => 0}
  numbers.each do |num|
    if num.odd?
      result[:odd] += 1
    else
      result[:even] += 1
    end
  end
  result
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  hash = common_vowels(string)
  max_value = find_max_value(hash)
  selected = hash.select {|k,v| v == max_value}
  selected.keys.sort.first
end

def common_vowels(string)
  vowels = "aeiou"
  hash = {}
  string.chars do |char|
    if hash[char] == nil && vowels.include?(char)
      hash[char] = 1
    elsif hash[char] != nil && vowels.include?(char)
      hash[char] += 1
    end
  end
  hash
end

def find_max_value(hash)
  max_value = 0
  hash.each do |k,v|
    if v > max_value
      max_value = v
    end
  end
  max_value
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  result = []
  array_of_values = find_right_values(students)
  i = 0
  while i < array_of_values.length - 1
    j = i + 1
    while j < array_of_values.length
      result << [array_of_values[i], array_of_values[j]]
      j += 1
    end
    i += 1
  end
  result
end

def find_right_values(students)
  new_hash = students.select {|k,v| v > 6 && v < 13}
  new_hash.keys
end



# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  hash = {}
  specimens.each do |ele|
    if hash[ele] == nil
      hash[ele] = 1
    else
      hash[ele] += 1
    end
  end
  smallest = smallest_population_size(hash)
  largest = largest_population_size(hash)
  (hash.length ** 2) * smallest / largest
end

def smallest_population_size(hash)
  if hash.length == 1
    return 1
  else
    return hash.values.min
  end
end

def largest_population_size(hash)
  if hash.length == 1
    return 1
  else
    return hash.values.max
  end
end
# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_hash = character_count(normal_sign)
  vand_sign_hash = character_count(vandalized_sign)
  vand_sign_hash.each do |k,v|
    normal_sign_hash.each do |key, value|
      if k == key && v > value
        return false
      end
    end
  end
  true
end

def character_count(str)
  hash = {}
  str.split(' ').join('').chars do |char|
    if hash[char] == nil
      hash[char] = 1
    else
      hash[char] += 1
    end
  end
  hash
end
